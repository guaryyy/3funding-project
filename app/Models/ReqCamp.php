<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReqCamp extends Model
{
    use HasFactory;

    protected $fillable = ['judul_campain', 'user_id', 'banner', 'proposal', 'target', 'deskripsi', 'status', 'no_rekening', 'payment_method'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
