<?php

namespace App\Http\Controllers;

use App\Models\Campain;
use App\Models\HistoryCampain;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(Campain $campain)
    {
        $payments = HistoryCampain::where('campain_id', $campain->id)->get();

        return view('payment.index', compact('payments'));
    }
}
