<?php

namespace App\Http\Controllers\API\user;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Validation\Validator;

class UserController extends Controller
{

    public function update(Request $request, $userId)
    {
        try {
            $user = User::findOrFail($userId);

            $validated = $request->validate([
                'name' => 'required|string',
                'password' => 'required|string',
                'email' => 'required|string|email',
            ]);

            if ($request->password) {
                $validated['password'] = bcrypt($request->password);
            }

            if ($user->email !== $request->email) {
                // Validasi email hanya jika emailnya berbeda
                $request->validate([
                    'email' => 'unique:users'
                ]);
            }

            $user->update($validated);

            return response()->json([
                'message' => 'User updated successfully',
                'data' => $user
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err,
            ]);
        }
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();
            return response()->json([
                "message" => "User deleted successfully",
            ]);
        } catch (Exception $err) {
            return response()->json([
                "message" => "Something went wrong",
                "data" => $err
            ]);
        }
    }
    public function show($userId)
    {
        try {
            $user = User::findOrFail($userId);
            if (!$user) {
                return response()->json([
                    'message' => "User nor found",
                ], 404);
            }
            return response()->json([
                'message' => 'Success mengambil data user',
                'data' => $user
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => "something went wrong",
                'data' => $err
            ], 500);
        }
    }

    public function allUsers()
    {
        try {
            $users = User::all();

            return response()->json([
                "message" => "Get All users successfully",
                'data' => $users
            ]);
        } catch (Exception $err) {
            return response()->json([
                "message" => "Something went wrong",
                'data' => $err
            ], 500);
        }
    }
}
