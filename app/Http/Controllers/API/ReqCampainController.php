<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\User;
use App\Models\ReqCamp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ReqCampainController extends Controller
{
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'judul_campain' => 'required|string',
                'proposal' => 'required|file|mimes:pdf',
                'target' => 'required',
                'deskripsi' => 'required',
                'status' => 'in:Pending,Verifikasi Berkas,Lolos Seleksi Pemberkasan,Lolos Seleksi Wawancara,Pengajuan Disetujui,Proses Published Campaign,Start Published Campain,End Published Campain,Proses Pencairan,Pencairan Telah Dikirim,Campain Selesai',
                'no_rekening' => 'required|string',
                'banner' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'payment_method' => 'required|string',
            ]);
            $validated['user_id'] = auth()->user()->id;
            $validated['proposal'] = $request->file('proposal')->store('public/proposals');
            $validated['banner'] = $request->file('banner')->store('public/img-banner-campains');
            // return $validated;
            $reqCampain = ReqCamp::create($validated);
            return response()->json([
                'message' => 'Request campain created successfully',
                'data' => $reqCampain
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, ReqCamp $reqCamp)
    {
        try {
            $validated = $request->validate([
                'judul_campain' => 'required|string',
                'proposal' => 'required|file|mimes:pdf',
                'target' => 'required',
                'deskripsi' => 'required',
                'no_rekening' => 'required',
                'payment_method' => 'required|string',
                'status' => 'in:Pending,Verifikasi Berkas,Lolos Seleksi Pemberkasan,Lolos Seleksi Wawancara,Pengajuan Disetujui,Proses Published Campaign,Start Published Campain,End Published Campain,Proses Pencairan,Pencairan Telah Dikirim,Campain Selesai'
            ]);

            if ($request->file('proposal') !== $reqCamp->proposal) {
                Storage::delete($reqCamp->proposal);
                $validated['proposal'] = $request->file('proposal')->store('proposals');
            }

            $reqCampain = $reqCamp->update($request->all());

            return response()->json([
                'message' => 'Request campain created successfully',
                'data' => $reqCampain
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function destroy(ReqCamp $reqCamp)
    {
        try {
            Storage::delete($reqCamp->proposal);
            $reqCamp->delete();
            return response()->json([
                'message' => 'request campain deleted successfully',
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function show(ReqCamp $reqCamp)
    {
        try {
            $reqCamp = $reqCamp->load('user');
            return response()->json([
                'message' => 'get request campain successfully',
                'data' => $reqCamp
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'Something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function reqCampByUser($userId)
    {
        try {

            $reqCamp = ReqCamp::with('user')->where('user_id', $userId)->get();

            return response()->json([
                'message' => 'date geted successfully',
                'data' => $reqCamp
            ]);
        } catch (Exception $err) {
            return response()->json([
                "message" => "Something went wrong",
                'data" => $err'
            ], 500);
        }
    }

    public function allReqCampains()
    {
        try {
            $reqCampains = ReqCamp::with('user')->orderBy('created_at', 'desc')->get();
            return response()->json([
                'message' => 'Get all request campains successfully',
                'data' => $reqCampains
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'Something went wrong',
                'data' => $err
            ], 500);
        }
    }
}
