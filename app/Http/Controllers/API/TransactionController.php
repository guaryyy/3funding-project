<?php

namespace App\Http\Controllers\API;

use Exception;
use Illuminate\Http\Request;
use Midtrans\Snap;
use Midtrans\Config;
use App\Models\HistoryCampain;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentController;
use App\Models\Campain;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{

    public function __construct()
    {
        \Midtrans\Config::$serverKey = config('services.midtrans.serverKey');
        \Midtrans\Config::$isProduction = config('services.midtrans.isProduction');
        \Midtrans\Config::$isSanitized = config('services.midtrans.isSanitized');
        \Midtrans\Config::$is3ds = config('services.midtrans.is3ds');
    }

    public function store(Request $request, $id)
    {
        try {
            $validated = $request->validate([
                'nominal_donatur' => 'required|min:0',
                'nama_donatur' => 'required|string'
            ]);

            $id_last = HistoryCampain::latest('id')->first();

            $validated['campain_id'] = $id;
            $validated['payment_method'] = "Midtrans";
            $validated['status_payment'] = "Unpaid";


            $params = array(
                'transaction_details' => array(
                    'order_id' => $id_last->id + 1,
                    'gross_amount' => $validated['nominal_donatur'],
                ),
                'customer_details' => array(
                    'first_name' => $validated['nama_donatur'],
                    'last_name' => 'R',
                    'email' => '3funding@example.com',
                    'phone' => '08111222333',
                ),
            );
            // return response()->json($params);

            $snapToken = \Midtrans\Snap::getSnapToken($params);
            $validated['token_transaction'] = $snapToken;

            $transaction = HistoryCampain::create($validated);
            return response()->json([
                'message' => 'transaction created',
                'data' => $transaction,
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function update(Request $request, HistoryCampain $historyCampain)
    {
        try {
            $validated = $request->validate([
                'payment_method' => 'required|string',
                'status_payment' => 'required|string'
            ]);

            $transaction = $historyCampain->update($validated);

            return response()->json([
                'message' => 'transaction updated successfully',
                'data' => $transaction
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err->getMessage()
            ], 500);
        }
    }

    public function destroy(HistoryCampain $history)
    {
        try {
            $history->delete();
            return response()->json([
                'message' => 'transaction deleted successfully',
                'history' => $history
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function Transaction($id)
    {
        try {
            $transactions = HistoryCampain::where('id', $id)->first();
            $campaign = Campain::where('id', $transactions['campain_id'])->first();
            return response()->json([
                'message' => 'Get Transaction successfully',
                'data' => $transactions,
                'campaign' => $campaign
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function callback(Request $request)
    {
        $server_key = config('services.midtrans.serverKey');
        $hashed = hash('sha512', $request->order_id . $request->status_code . $request->gross_amount . $server_key);
        if ($hashed == $request->signature_key) {
            if ($request->transaction_status == "capture") {
                $transaction = HistoryCampain::find($request->order_id);
                $transaction->update(['status_payment' => 'Success'], ['payment_method' => $request->payment_type]);
            }
        }
    }
}
