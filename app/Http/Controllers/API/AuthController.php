<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'email' => 'required|string|unique:users|email',
                'password' => 'required|string',
                'institusi' => 'required|string',
                'phone_number' => 'required|string'
            ]);
            // $validator['role'] = 'user';

            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json([
                    "message" => "Bad request",
                    "errors" => $errors
                ], 400);
            }

            $validated = $validator->validated();
            $validated["password"] = bcrypt($request->password);
            $user = User::create($validated);
            return response()->json([
                "message" => "Registration successful",
                "data" => $user
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                "message" => "something went wrong",
                "data" => $err,
            ], 500);
        }
    }

    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string',
            ]);

            $user = User::where('email', $request->email)->first();

            if (!$user || !Hash::check($request->password, $user->password)) {
                return response()->json([
                    "message" => "username or password wrong"
                ], 400);
            }

            $token = $user->createToken('success login')->plainTextToken;
            return response()->json([
                "message" => "Success login",
                "token" => $token
            ]);
        } catch (Exception $err) {
            return response()->json([
                "message" => "Something went wrong",
                "data" => $err
            ], 500);
        }
    }


    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();

            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err->getMessage()
            ], 500);
        }
    }
}
