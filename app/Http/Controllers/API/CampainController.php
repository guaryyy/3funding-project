<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Campain;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\HistoryCampain;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class CampainController extends Controller
{
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title_campains' => 'required|string', // Hapus karakter '|' yang ekstra di sini
                'banner_campains' => 'required|image',
                'deskripsi' => 'required|string',
                'start_date_camp' => 'required', // Hapus karakter ',' yang ekstra di sini
                'end_date_camp' => 'required' // Hapus karakter ',' yang ekstra di sini
            ]);

            $validated['banner_campains'] = $request->file('banner_campains')->store('banner-campains');
            $campain = Campain::create($validated);

            return response()->json([
                'message' => 'Campaign created successfully',
                'data' => $campain
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'Something went wrong',
                'data' => $err->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, Campain $campain)
    {
        try {
            $validated = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title_campains' => 'required|string|',
                'banner_campains' => 'required|image',
                'deskripsi' => 'required|string',
                'start_date_camp' => 'required|',
                'end_date_camp' => 'required,'
            ]);

            if ($request->file('banner_campain') !== $campain->banner_campains) {
                Storage::delete($campain->banner_campains);
                $validated['banner_campains'] = $request->file('banner_campains')->store('banner-campains');
            }
            $campain = $campain->update($validated);

            return response()->json([
                'message' => 'campain updated successfully',
                'data' => $campain
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function destroy(Campain $campain)
    {
        try {
            Storage::delete($campain->banner_campains);
            $campain->delete();
            return response()->json([
                'message' => 'campain deleted successfully',
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err
            ], 500);
        }
    }

    public function show(Campain $campain)
    {
        try {
            $campain = $campain->load(['historycampains', 'user']);

            // Menghitung total nominal donatur
            $totalNominalDonatur = $campain->historycampains->where('status_payment', 'Success')->sum('nominal_donatur');
            $transactions = $campain->historycampains()
                ->where('status_payment', 'Success')
                ->paginate(3);

            return response()->json([
                'message' => 'Data campain successfully',
                'data' => [
                    'id' => $campain->id,
                    'user' => User::find($campain->user_id), // Mengambil data pengguna
                    'title' => $campain->title_campains,
                    'image' => $campain->banner_campains,
                    'status' => $campain->status,
                    'description' => $campain->deskripsi,
                    'target' => $campain->target_campains,
                    'transaction' => $transactions,
                    'start_date_camp' => $campain->start_date_camp,
                    'end_date_camp' => $campain->end_date_camp,
                    'total_nominal_donatur' => $totalNominalDonatur,
                ],
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => "something went wrong",
                'data' => $err
            ], 500);
        }
    }


    public function mainCampain()
    {
        try {
            $campains = Campain::get();

            $campaignIds = $campains->pluck('id');

            $totalPayments = HistoryCampain::selectRaw('campain_id, SUM(nominal_donatur) as total_payment')
                ->whereIn('campain_id', $campaignIds)
                ->groupBy('campain_id')
                ->get();

            $totalPayment = [];
            foreach ($totalPayments as $payment) {
                $totalPayment[$payment->campain_id] = $payment->total_payment;
            }



            return response()->json([
                'message' => 'Campain get successfuly',
                'data' => $totalPayment
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => "something went wrong",
                'data' => $err
            ], 500);
        }
    }

    public function campainList()
    {
        try {
            // Mengambil semua kampanye
            $campains = Campain::with('historycampains')->get();

            // Menghitung total nominal donatur untuk setiap kampanye
            $data = $campains->map(function ($campain) {
                $totalNominalDonatur = $campain->historycampains->where('status_payment', 'Success')->sum('nominal_donatur');

                return [
                    'id' => $campain->id,
                    // 'user_id' => User::where('id', $campain->user_id)->firstt(),
                    'user' => User::where('id', $campain->user_id)->first(),

                    'title' => $campain->title_campains,
                    'image' => $campain->banner_campains,
                    'status' => $campain->status,
                    'description' => $campain->deskripsi,
                    'target' => $campain->target_campains,
                    'start_date_camp' => $campain->start_date_camp,
                    'end_date_camp' => $campain->end_date_camp,
                    'total_nominal_donatur' => $totalNominalDonatur
                ];
            });

            return response()->json([
                'message' => 'all campains get successfully',
                'data' => $data
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err->getMessage()
            ], 500);
        }
    }

    public function campainByUser($userId)
    {
        try {
            $campains = Campain::with(['historycampains'])->where('user_id', $userId)->get();

            $data = $campains->map(function ($campain) {
                $totalDonate = $campain->historycampains->where('status_payment', 'success')->sum('nominal_donatur');
                return [
                    'id' => $campain->id,
                    'title' => $campain->title_campains,
                    'image' => $campain->banner_campains,
                    'status' => $campain->status,
                    'description' => $campain->deskripsi,
                    'target' => $campain->target_campains,
                    'start_date_camp' => $campain->start_date_camp,
                    'end_date_camp' => $campain->end_date_camp,
                    'total_nominal_donatur' => $totalDonate,
                    'transaction' => $campain->historycampains
                ];
            });

            return response()->json([
                'message' => 'campain by user get success',
                'data' => $data
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => "something went wrong",
                'data' => $err
            ], 500);
        }
    }
    public function withdraw($id)
    {
        try {
            $historyCampain = HistoryCampain::where('campain_id', $id)->first();
            if (!$historyCampain) {
                return response()->json([
                    'message' => 'campain you want to withdraw notfound'
                ]);
            }
            $historyCampain->update([
                'nominal_donatur' => '0'
            ]);

            return response()->json([
                'message' => 'withdraw successful'
            ]);
        } catch (Exception $err) {
            return response()->json([
                'message' => 'something went wrong',
                'data' => $err->getMessage(),
            ]);
        }
    }

    public function search($key)
    {
        $campaign = Campain::where('title_campains', 'LIKE', '%' . $key . '%')->get();
        $data = $campaign->map(function ($campain) {
            $totalNominalDonatur = $campain->historycampains->where('status_payment', 'Success')->sum('nominal_donatur');

            return [
                'id' => $campain->id,
                'user' => User::where('id', $campain->user_id)->first(),
                'title' => $campain->title_campains,
                'image' => $campain->banner_campains,
                'status' => $campain->status,
                'description' => $campain->deskripsi,
                'target' => $campain->target_campains,
                'start_date_camp' => $campain->start_date_camp,
                'end_date_camp' => $campain->end_date_camp,
                'total_nominal_donatur' => $totalNominalDonatur
            ];
        });
        return $data;
    }
}
