<?php

namespace App\Http\Controllers;

use App\Models\Campain;
use App\Models\ReqCamp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RekapController extends Controller
{
    public function index()
    {
        $datas = ReqCamp::get();
        $checked = Campain::get();

        return view('datauser.index', compact('datas', 'checked'));
    }

    public function update(Request $request, ReqCamp $reqcamp)
    {
        $reqcamp->update($request->all());

        return redirect('/data-users')->with('success', 'The statuses user campains successfully updated');
    }

    public function show(User $user)
    {

        return view('datauser.show', compact('user'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'banner_campains' => ['image', 'max:3000'],
        ]);

        Campain::create([
            'user_id' => $request->user_id,
            'title_campains' => $request->title_campains,
            'deskripsi' => $request->deskripsi,
            'start_date_camp' => $request->start_date_camp,
            'end_date_camp' => $request->end_date_camp,
            'target_campains' => $request->target_campains,
            'banner_campains' => $request->file('banner_campains')->store('img-banner-campains'),
        ]);

        return redirect('/data-users')->with('success', 'The Campains Has Been Added');
    }

    public function downloadProposal(ReqCamp $reqcamp)
    {
        return Storage::download($reqcamp->proposal);
    }
}
