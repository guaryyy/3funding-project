<?php

namespace App\Http\Controllers;

use App\Models\Campain;
use App\Models\HistoryCampain;
use Illuminate\Http\Request;

class CampainController extends Controller
{
    public function index()
    {
        $campains = Campain::get();

        $campaignIds = $campains->pluck('id');

        $totalPayments = HistoryCampain::selectRaw('campain_id, SUM(nominal_donatur) as total_payment')
            ->whereIn('campain_id', $campaignIds)
            ->groupBy('campain_id')
            ->get();

        $totalPayment = [];
        foreach ($totalPayments as $payment) {
            $totalPayment[$payment->campain_id] = $payment->total_payment;
        }

        return view('datacampain.index', compact('campains', 'totalPayment'));
    }


    public function edit(Campain $campain)
    {

        return view('datacampain.edit', compact('campain'));
    }

    public function update(Request $request, Campain $campain)
    {
        $validates = $request->validate([
            'title_campains' => ['min:3'],
            'deskripsi' => ['min:3'],
            'banner_campains' => ['image', 'max:3000'],
            'start_date_camp' => ['date'],
            'end_date_camp' => ['date'],
            'target_campains' => ['min:3'],
            'status_campains' => ['min:3'],
        ]);

        $validates['banner_campains'] = $request->file('banner_campains')->store('img-banner-campains');

        $campain->update($validates);

        return redirect('/data-campains')->with('success', 'The Campains Has Been Updated Successfully');
    }



    public function destroy(Campain $campain)
    {
        $campain->delete();
        return back()->with('success', 'The Campains Has Been Deleted Successfully');
    }

    public function searchCampainByAuthor(Request $request)
    {
        $query = $request->input('query');
        $campains = Campain::where('title_campains', 'LIKE', '%' . $query . '%')->get();
        $campaignIds = $campains->pluck('id');

        $totalPayments = HistoryCampain::selectRaw('campain_id, SUM(nominal_donatur) as total_payment')
            ->whereIn('campain_id', $campaignIds)
            ->groupBy('campain_id')
            ->get();

        $totalPayment = [];
        foreach ($totalPayments as $payment) {
            $totalPayment[$payment->campain_id] = $payment->total_payment;
        }

        return view('datacampain.index', compact('campains', 'totalPayment'));
    }
}
