<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Campain;
use App\Models\HistoryCampain;
use App\Models\ReqCamp;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [];
        foreach (range(1, 12) as $month) {
            $kunjungan = Campain::whereMonth('start_date_camp', $month)->count();
            $data[$month] = $kunjungan;
        }

        $user = [];

        $user1 = User::where('roles', 'user')->count();
        $admin = User::where('roles', 'admin')->count();

        $users[0] = $user1;
        $users[1] = $admin;

        $datas = [];

        $dana = HistoryCampain::where('payment_method', 'DANA')->count();
        $bni = HistoryCampain::where('payment_method', 'BNI')->count();
        $credit_card = HistoryCampain::where('payment_method', 'credit_card')->count();
        $bca = HistoryCampain::where('payment_method', 'BCA')->count();
        $lain = HistoryCampain::where('payment_method', '!=', 'DANA')->count();

        $datas[0] = $bca;
        $datas[1] = $dana;
        $datas[2] = $bni;
        $datas[3] = $credit_card;
        $datas[4] = $lain;

        return view('dashboard.dashboard', compact('data', 'datas', 'users'));
    }
}
