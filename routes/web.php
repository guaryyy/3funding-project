<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CampainController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\RekapController;
use Faker\Provider\ar_EG\Payment;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'index')->name('login');
    Route::post('/', 'login')->name('loginPost');
    Route::get('/logout', 'logout')->name('logout');
});

Route::middleware('authenticated')->group(function () {
    // Dashboard Route
    Route::middleware('auth-roles:admin')->group(function () {
        Route::controller(DashboardController::class)->group(function () {
            Route::get('/dashboard', 'index')->name('dashboard');
        });

        // datas user campains
        Route::controller(RekapController::class)
            ->prefix('data-users')
            ->name('data.')
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::patch('/{reqcamp}/updated', 'update')->name('update');
                Route::get('/{user}/make-campain/create', 'show')->name('show');
                Route::post('/make-campain', 'store')->name('store');
                // route donwload proposals 
                Route::get('/{reqcamp}/download', 'downloadProposal')->name('download');
            });

        // datas users campains approved
        Route::controller(CampainController::class)
            ->prefix('data-campains')
            ->name('campain.')
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/{campain}/editted', 'edit')->name('edit');
                Route::patch('/{campain}/editted', 'update')->name('update');
                Route::get('/{campain}/deleted', 'destroy')->name('destroy');
                Route::get('/search/success', 'searchCampainByAuthor')->name('search');
            });

        // datas users campains payments
        Route::controller(PaymentController::class)
            ->prefix('data-history-payment')
            ->name('payment.')
            ->group(function () {
                Route::get('/{campain}/get-datas', 'index')->name('index');
            });
    });
});
