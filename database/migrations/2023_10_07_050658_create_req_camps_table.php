<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_camps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('judul_campain');
            $table->string('proposal');
            $table->string('target');
            $table->string('banner');
            $table->text('deskripsi');
            $table->string('no_rekening');
            $table->string('payment_method');
            $table->enum('status', ['Pending', 'Verifikasi Berkas', 'Lolos Seleksi Pemberkasan', 'Lolos Seleksi Wawancara', 'Pengajuan Disetujui', 'Proses Published Campaign', 'Start Published Campain', 'End Published Campain', 'Proses Pencairan', 'Pencairan Telah Dikirim', 'Campain Selesai'])->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('req_camps');
    }
};
