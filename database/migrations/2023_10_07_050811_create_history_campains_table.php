<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_campains', function (Blueprint $table) {
            $table->id();
            $table->foreignId('campain_id');
            $table->string('nama_donatur')->default('annonymous');
            $table->string('nominal_donatur');
            $table->string('payment_method')->default('BCA');
            $table->string('token_transaction')->nullable();
            $table->string('status_payment')->default('Process');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_campains');
    }
};
