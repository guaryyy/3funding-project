@extends('templates.master')
@section('title', 'Make Campains ')
@section('page-name', 'Make Campains')
@push('styles')
@endpush
@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="container">
                    <div class="card-header">
                        <h1 style="font-family: 'Montserrat', sans-serif;">Added Campains For User {{ $user->name }}
                        </h1>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('data.store') }}" method="post" class="form form-vertical"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group mb-3">
                                                <h6 class="card-title"
                                                    style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                    Title Campain</h6>
                                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                <input type="text" name="title_campains" id="contact-info-vertical"
                                                    class="form-control" name="contact" placeholder="title campains">
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Deskripsi Campain</h6>
                                            <textarea name="deskripsi" id="" cols="10" class="form-control"
                                                placeholder="deskripsi campain">

                                                </textarea>
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Banner Campain</h6>
                                            <input type="file" name="banner_campains" class="form-control" id="">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Start Date Campains</h6>
                                            <input type="date" name="start_date_camp" id="password-vertical"
                                                class="form-control" placeholder="start date camp">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                End Date Campains</h6>
                                            <input type="date" name="end_date_camp" id="password-vertical"
                                                class="form-control" placeholder="end date camp">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Target Campains</h6>
                                            <input type="date" name="target_campains" id="password-vertical"
                                                class="form-control" placeholder="end date camp">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Added</button>

                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
@push('scripts')
@endpush