@extends('templates.master')
@section('title', 'Detail Data User')
@section('page-name', 'Detail Data User')
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')

<section class="section">
    <div class="card">
        <div class="container ">
            <div class="card-header">
                {{-- <select class="form-select w-25" id="sortingSelect">
                    <option value="{{ route('rawatjalan.index') }}" {{ request()->is('rawat-jalan/detail-kunjungan')
                        ? 'selected disabled' : '' }}>Semua Data Kunjungan</option>
                    <option value="{{ route('rawatjalan.sudahPemeriksaan') }}" {{ request()->
                        is('rawat-jalan/detail-kunjungan/sudah-pemeriksaan') ? 'selected disabled' : '' }}>Sudah
                        Pemeriksaan</option>
                    <option value="{{ route('rawatjalan.belumPemeriksaan') }}" {{ request()->
                        is('rawat-jalan/detail-kunjungan/belum-pemeriksaan') ? 'selected disabled' : '' }}>Belum
                        Pemeriksaan</option>
                </select> --}}
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th>ID Req</th>
                        <th>Nama</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Judul Campain</th>
                        <th>Proposal</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($datas as $data)
                    <tr>
                        <td>#00{{ $data->id }}</td>
                        <td>{{ $data->user->name }}</td>
                        <td>{{ date('d F Y', strtotime($data->created_at)) }}</td>
                        <td>{{ $data->judul_campain }}</td>
                        <td>
                            {{-- @if($data->proposal == null) --}}
                            {{-- <button class="btn btn-sm btn-secondary" disabled><i class="bi bi-download"></i></button> --}}
                            {{-- @else  --}}
                            <a href="{{ route('data.download', $data->id) }}"><i class="bi bi-download"></i></a>
                            {{-- <button type="submit" class="btn btn-sm btn-danger"></button> --}}
                            {{-- @endif --}}
                        </td>
                        @if($data->status == 'Pending' || $data->status == 'Verifikasi Berkas')
                        <td>
                            <p class="badge bg-danger">
                                {{ $data->status }}
                            </p>
                        </td>
                        @else
                        <td>
                            <p class="badge bg-success">
                                {{ $data->status }}
                            </p>
                        </td>

                        @endif

                        <td>
                            {{-- <form action="{{ route('rawatjalan.destroy', $detailkunjungan) }}" method="post"> --}}
                                <button type="button" class="btn btn-sm btn-primary block" data-bs-toggle="modal"
                                    data-bs-target="#exampleModalCenter{{ $data->id }}">
                                    <i class="fa-solid fa-eye"></i>
                                </button>
                                <div class="modal fade" id="exampleModalCenter{{ $data->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                                        role="document">
                                        <div class="modal-content">
                                            <div class="modal-header" style="padding: 20px;">
                                                <h2 class="modal-title" id="exampleModalCenterTitle">Detail data user
                                                    campains</h2>
                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                    aria-label="Close">
                                                    <i data-feather="x"></i>
                                                </button>
                                            </div>

                                            <div class="modal-body" style="max-height: 450px; overflow-y: auto;">
                                                <h6 style="font-size: 16px;">Identitas User</h6>
                                                <div class="row">
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Nama </p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $data->user->name }}
                                                        <p>
                                                    </div>
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Judul Campain </p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $data->judul_campain }}
                                                        <p>
                                                    </div>
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Tanggal Pengajuan</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ date('d F Y',
                                                            strtotime($data->created_at))
                                                            }}
                                                        </p>
                                                    </div>
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Institusi</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $data->user->institusi}}
                                                        </p>
                                                    </div>
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">No Rekening</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $data->no_rekening}}
                                                        </p>
                                                    </div>
                                                </div>
                                                <h6 style="font-size: 16px; margin-top: 7px;">Detail Data Pengajuan</h6>
                                                <div class="row">
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Target</p>
                                                    </div>

                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">
                                                            {{ $data->target }}</p>
                                                    </div>

                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Deskripsi</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $data->deskripsi }}</p>
                                                    </div>


                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Status</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">
                                                            {{$data->status}}</p>
                                                    </div>

                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Proposal File</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">
                                                            {{ $data->proposal }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="padding: 10px;">
                                                <button type="button" class="btn btn-light-secondary"
                                                    data-bs-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Close</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- modal editted --}}
                                <button type="button" class="btn btn-sm btn-warning block" data-bs-toggle="modal"
                                    data-bs-target="#exampleModalCenter{{ $data->user->id }}">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>

                                <div class="modal fade" id="exampleModalCenter{{ $data->user->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable "
                                        role="document">
                                        <div class="modal-content">
                                            <div class="modal-header" style="padding: 20px;">
                                                <h2 class="modal-title" id="exampleModalCenterTitle">Edit Status User
                                                    Campains</h2>
                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                    aria-label="Close">
                                                    <i data-feather="x">x</i>
                                                </button>
                                            </div>

                                            <div class="modal-body" style="max-height: 450px; overflow-y: auto;">
                                                <form action="{{ route('data.update', $data) }}" method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <div class="">
                                                        <p style="">Status </p>
                                                    </div>
                                                    <div class="mb-3">
                                                        <select name="status" class="form-select w-100"
                                                            id="sortingSelect">
                                                            <option value="Pending" {{ ($data->status == 'Pending') ?
                                                                'selected' : '' }}>Pending</option>
                                                            <option value="Verifikasi Berkas">Verifikasi Berkas</option>
                                                            <option value="Lolos Seleksi Pemberkasan">Lolos Seleksi
                                                                Pemberkasan</option>
                                                            <option value="Lolos Seleksi Wawancara">Lolos Seleksi
                                                                Wawancara
                                                            </option>
                                                            <option value="Pengajuan Disetujui">Pengajuan Disetujui
                                                            </option>
                                                            <option value="Proses Published Campain" {{ ($data->status
                                                                == 'Proses Published Campain') ? 'selected' : ''
                                                                }}>Proses Published
                                                                Campain</option>
                                                            <option value="Start Published Campain">Start Published
                                                                Campain
                                                            </option>
                                                            <option value="End Published Campain">End Published Campain
                                                            </option>
                                                            <option value="Proses Pencairan">Proses Pencairan</option>
                                                            <option value="Pencairan Telah Dikirim">Pencairan Telah
                                                                Dikirim
                                                            </option>
                                                            <option value="Campain Selesai">Campain Selesai</option>
                                                        </select>
                                                    </div>

                                                    <!-- Move the submit button to the right -->

                                                    <div class="modal-footer" style="padding: 10px;">
                                                        <button type="button" class="btn btn-light-secondary"
                                                            data-bs-dismiss="modal">
                                                            <i class="bx bx-x d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Close</span>
                                                        </button>
                                                        <button type="submit"
                                                            class="btn btn-primary float-end">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @if($data->status == 'Proses Published Campaign')
                                <a class="btn btn-sm btn-success block" href="{{ route('data.show', $data->user_id) }}">
                                    <i class="fa-solid fa-briefcase-medical"></i>
                                </a>
                                @else
                                <button class="btn btn-sm btn-secondary" disabled><i
                                        class="fa-solid fa-briefcase-medical"></i></button>
                                @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
</div>


<script>
    document.getElementById('sortingSelect').addEventListener('change', function() {
        var selectedOption = this.options[this.selectedIndex];
        var route = selectedOption.value;

        if (route) {
            window.location.href = route;
        }
    });
</script>

<script src="{{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
<script>
    let dataTable = new simpleDatatables.DataTable(
                    document.getElementById("table1")
                )               
</script>

<script src="assets/js/main.js"></script>

@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(session()->has('success'))
<script>
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text : "{{ session('success') }}",
    showConfirmButton: true,
    timer: 2000
    });
</script>
@endif
@endpush