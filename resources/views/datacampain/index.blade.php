@extends('templates.master')
@section('title', 'Detail Data Campains')
@section('page-name', 'Detail Data Campains')
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')

<form action="{{ route('campain.search') }}" method="GET">
    <div class="d-flex my-4 justify-content-end">
        <input type="text" class="form-control form-control-sm w-25" name="query" placeholder="Search Campains by title"
            aria-label="Search...">
        <button class="btn btn-sm btn-warning mx-2" type="submit">
            <i class="fa-solid fa-search"></i>
        </button>
    </div>
</form>
<div class="row">
    @foreach($campains as $campain)

    <div class="col-md-4">
        <div class="card">
            <div class="card-content">
                <div class="w-100" style="height: 150px; overflow: hidden;">
                    <img src="{{ asset('storage/' . $campain->banner_campains) }}" class="card-img-top img-fluid"
                        alt="singleminded">
                </div>
                <div class="card-body">
                    <div class="d-flex">
                        <h3>{{ $campain->title_campains }}</h3>
                        @if($campain->status_campains == 'Open')
                        <span class="text-primary">{{ $campain->status_campains }}</span>
                        @else
                        <span class="text-danger">{{ $campain->status_campains }}</span>
                        @endif
                    </div>
                    <p class="fw-bolder">Author : {{ $campain->user->name }}</p>
                    {{-- <p class="card-text text-danger">
                        Start Date : {{ date('d F Y',
                        strtotime($campain->start_date_camp))
                        }} - {{ date('d F Y',
                        strtotime($campain->end_date_camp))}}
                    </p>
                    <p class="fw-bolder">Total Donatur All :
                        Rp {{ $totalPayment[$campain->id] ?? 0 }}
                    </p>
                    <p class="fw-bolder">Target Dana Campain :
                        Rp {{ $campain->target_campains }}
                    </p> --}}
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-3">

                        <button type="button" class="btn btn-sm btn-primary block" data-bs-toggle="modal"
                            data-bs-target="#exampleModalCenter{{ $campain->id }}">
                            <i class="fa-solid fa-eye"></i>
                        </button>
                        <div class="modal fade" id="exampleModalCenter{{ $campain->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="padding: 20px;">
                                        <h2 class="modal-title" id="exampleModalCenterTitle">Detail campains</h2>
                                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>

                                    <div class="modal-body" style="max-height: 450px; overflow-y: auto;">
                                        <h6 class="text-center" style="font-size: 16px;">{{ $campain->title_campains }}
                                        </h6>
                                        <div class="row">
                                            <div class="col-5">
                                                <p style="font-size: 14px;">Nama Team</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">{{ $campain->user->name }}
                                                </p>
                                            </div>
                                            <div class="col-5">
                                                <p style="font-size: 14px;">Deskripsi Campaigns</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">{{ $campain->deskripsi }}
                                                <p>
                                            </div>
                                            <div class="col-5">
                                                <p style="font-size: 14px;">Start Date</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">{{ date('d F Y',
                                                    strtotime($campain->start_date_camp))
                                                    }}
                                                </p>
                                            </div>
                                            <div class="col-5">
                                                <p style="font-size: 14px;">End Date</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">{{ date('d F Y',
                                                    strtotime($campain->end_date_camp))
                                                    }}
                                                </p>
                                            </div>
                                            <div class="col-5">
                                                <p style="font-size: 14px;">Total Donatur</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">
                                                    Rp {{ $totalPayment[$campain->id] ?? 0 }}
                                                </p>
                                            </div>
                                            <div class="col-5">
                                                <p style="font-size: 14px;">Target</p>
                                            </div>
                                            <div class="col-7">
                                                <p style="font-size: 14px;">
                                                    Rp {{ $campain->target_campains }}

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="padding: 10px;">
                                        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                            <i class="bx bx-x d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Close</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <a class="btn btn-sm btn-success" href="{{ route('payment.index', $campain) }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-coin" viewBox="0 0 16 16">
                                <path
                                    d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z" />
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                <path
                                    d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z" />
                            </svg>
                        </a>
                        {{-- <i class="fa-solid fa-eye"></i></a> --}}
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-sm btn-warning" href="{{ route('campain.edit', $campain) }}"><i
                                class="fa-solid fa-pencil"></i></a>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-sm btn-danger" href="{{ route('campain.destroy', $campain) }}"><i
                                class="fa-solid fa-trash"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>

<script>
    document.getElementById('sortingSelect').addEventListener('change', function() {
        var selectedOption = this.options[this.selectedIndex];
        var route = selectedOption.value;

        if (route) {
            window.location.href = route;
        }
    });
</script>

<script src="{{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
<script>
    let dataTable = new simpleDatatables.DataTable(
                    document.getElementById("table1")
                )               
</script>

<script src="assets/js/main.js"></script>

@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(session()->has('success'))
<script>
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text : "{{ session('success') }}",
    showConfirmButton: true,
    timer: 2000
    });
</script>
@endif
@endpush