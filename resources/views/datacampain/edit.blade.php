@extends('templates.master')
@section('title', 'Edit Campains ')
@section('page-name', 'Edit Campains')
@push('styles')
@endpush
@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="container">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('campain.update', $campain) }}" method="post"
                                class="form form-vertical" enctype="multipart/form-data">
                                @csrf
                                @method('patch')
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group mb-3">
                                                <h6 class="card-title"
                                                    style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                    Title Campain</h6>
                                                <input type="hidden" name="user_id" value="{{ $campain->user_id }}">
                                                <input type="text" name="title_campains" id="contact-info-vertical"
                                                    class="form-control" name="contact" placeholder="title campains"
                                                    value="{{ $campain->title_campains }}">
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                            style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                            Deskripsi Campain</h6>
                                                {{-- <textarea class="form-control" name="deskripsi" id="" cols="10" >
                                                    {{ $campain->deskripsi }}
                                                </textarea> --}}
                                                <input class="form-control" type="text" value="{{ $campain->deskripsi }}"
                                                name="deskripsi" class="form-control" id="">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Banner Campain</h6>
                                            <input class="form-control" type="file" value="{{ $campain->banner_campains }}"
                                                name="banner_campains" class="form-control" id="">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Start Date Campains</h6>
                                            <input type="date" value="{{ $campain->start_date_camp }}"
                                                name="start_date_camp" id="password-vertical" class="form-control"
                                                placeholder="start date camp">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                End Date Campains</h6>
                                            <input type="date" value="{{ $campain->end_date_camp }}"
                                                name="end_date_camp" id="password-vertical" class="form-control"
                                                placeholder="end date camp">
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Status Campains</h6>
                                            <select class="form-control" name="status_campains" id="">
                                                <option value="Open" {{ ($campain->status_campains) ==  'Open' ? 'selected' : ''}}>Opened Campains</option>
                                                <option value="Close" {{ ($campain->status_campains) ==  'Close' ? 'selected' : ''}}>Closed Campains</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Updated</button>

                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
@push('scripts')
@endpush