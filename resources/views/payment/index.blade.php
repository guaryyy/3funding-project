@extends('templates.master')
@section('title', 'Detail Payment Campains')
@section('page-name', 'Detail Payment Campains')
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')

<section class="section">
    <div class="card">
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th>Nama Donatur</th>
                        <th>Nominal Donatur</th>
                        <th>Payment Method</th>
                        <th>Status Payments</th>
                        <th>Tanggal Pengiriman</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $payment)
                    <tr>
                        <td>{{ $payment->nama_donatur }}</td>
                        <td>{{ $payment->reqcamp }}</td>
                        <td>Rp. {{ number_format($payment->nominal_donatur, 0, ',', '.') }}</td>
                        <td>{{ $payment->payment_method }}</td>
                        <td>
                            @if($payment->status_payment == 'Success')
                            <p class="badge bg-success">
                                {{ $payment->status_payment }}
                            </p>
                            @else
                            <p class="badge bg-danger">
                                {{ $payment->status_payment }}
                            </p>
                            @endif
                        </td>
                        <td>{{ date('d F Y', strtotime($payment->created_at)) }}</td>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
</div>


<script>
    document.getElementById('sortingSelect').addEventListener('change', function() {
        var selectedOption = this.options[this.selectedIndex];
        var route = selectedOption.value;

        if (route) {
            window.location.href = route;
        }
    });
</script>

<script src="{{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
<script>
    let dataTable = new simpleDatatables.DataTable(
                    document.getElementById("table1")
                )               
</script>

<script src="assets/js/main.js"></script>

@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(session()->has('success'))
<script>
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text : "{{ session('success') }}",
    showConfirmButton: true,
    timer: 2000
    });
</script>
@endif
@endpush