@extends('templates.master')
@section('title', 'Dashboard')
@section('page-name', 'Dashboard')
@push('styles')
@endpush
@section('content')
<div class="row mt-4">
    <div class="col-6 col-lg-5 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="text-center">Statistik Users 3FUNDING</h4>
            </div>
            <div class="card-body d-flex justify-content-center flex-col" id="chartKunjungans">

            </div>
        </div>
    </div>
    <div class="col-6 col-lg-4 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="text-center">Statistik Payments User Donate</h4>
            </div>
            <div class="card-body d-flex justify-content-center flex-col" id="chart">

            </div>
        </div>
    </div>

    <div class="col-6 col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <img src="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3270&q=80"
                        class="rounded-circle" style="width: 86px;height:86px;" alt="Avatar" />
                </div>
                <h5 class="font-bold text-center mt-3">Super Admin 3Funding</h5>
                <p class=" mb-0 text-center">{{ auth()->user()->name }}</p>
            </div>
        </div>
    </div>
</div>

<div class="col-6 col-lg-12 col-md-6">
    <div class="card">
        <div class="card-header">
            <h4 class="text-center">Statistic Campains</h4>
        </div>
        <div class="card-body" id="chartKunjungan">
            <p class="text-center">datas in campains</p>
        </div>
    </div>
</div>



</div>

@endsection
@push('scripts')

<script>
    var options2 = {
chart: {
type: 'bar'
},
series: [
{
  name: 'sales',
  data: [
    {!! json_encode($data[1], JSON_HEX_TAG) !!},
    {!! json_encode($data[2], JSON_HEX_TAG) !!},
    {!! json_encode($data[3], JSON_HEX_TAG) !!},
    {!! json_encode($data[4], JSON_HEX_TAG) !!},
    {!! json_encode($data[5], JSON_HEX_TAG) !!},
    {!! json_encode($data[6], JSON_HEX_TAG) !!},
    {!! json_encode($data[7], JSON_HEX_TAG) !!},
    {!! json_encode($data[8], JSON_HEX_TAG) !!},
    {!! json_encode($data[9], JSON_HEX_TAG) !!},
    {!! json_encode($data[10], JSON_HEX_TAG) !!},
    {!! json_encode($data[11], JSON_HEX_TAG) !!},
    {!! json_encode($data[12], JSON_HEX_TAG) !!},
    ]
}
],
xaxis: {
categories: ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
}
}


var chart = new ApexCharts(document.querySelector('#chartKunjungan'), options2)
chart.render()

var options = {
chart: {
type: 'pie',
width: 380,
},
series: [
    {!! json_encode($datas[0], JSON_HEX_TAG) !!},
    {!! json_encode($datas[1], JSON_HEX_TAG) !!},
    {!! json_encode($datas[2], JSON_HEX_TAG) !!},
    {!! json_encode($datas[3], JSON_HEX_TAG) !!},
    {!! json_encode($datas[4], JSON_HEX_TAG) !!},
],
labels: ['BCA', 'DANA', 'BNI', 'CREDIT CARD','LAINNYA'],
}

var chart = new ApexCharts(document.querySelector('#chart'), options )
chart.render()

var options1 = {
chart: {
type: 'pie',
width: 380,
},
series: [
    {!! json_encode($users[0], JSON_HEX_TAG) !!},
    {!! json_encode($users[1], JSON_HEX_TAG) !!},
],
labels: ['User', 'Admin'],
}

var chart = new ApexCharts(document.querySelector('#chartKunjungans'), options1)
chart.render()



</script>
@endpush